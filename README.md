# Theme Field

The Theme Field module provides a standard custom content field for selecting
a Drupal theme by name from a dropdown. With this module, a select field can
be added to any content entity type allowing content editors to pick a theme
from a list of active themes on your site. When a Theme Field is non-empty, the
specified theme will be used to render the _full page display_ of that entity.

Note: All enabled themes are available for selection, including base themes
from Drupal core like Classy and Stable. This could be useful for reviewing
your content against a basic Drupal installation, however these options may
be confusing for editors and not recommended in production environments.
To remove undesired themes see [#3033402: option to remove Core themes][3033402]

[3033402]: https://www.drupal.org/i/3033402


## Requirements

This module has no special requirements other than having at least two Drupal
themes enabled. If no themes are enabled, the dropdown will appear empty.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal Modules][].

[Installing Drupal Modules]: https://www.drupal.org/docs/extending-drupal/installing-drupal-modules


## Configuration

1.  Ensure you have one or more Drupal themes enabled on your site.

1.  In the "Manage Fields" tab of your content type, add a Theme Field to one
    or more of your node types. This will enable editors to specify a theme
    for each individual node.

1.  Create or edit a node with a Theme Field; select a different theme.


## Recommended modules

[Paragraphs LandingPage Framework][] - The Theme Field module was developed as
an offshoot from this module because it was found to be useful to have a
generic way to change themes not only inside Landing Pages.

[Paragraphs LandingPage Framework]: https://www.drupal.org/project/landingpage


## Maintainers

* James Wilson - [jwilson3](https://www.drupal.org/u/jwilson3)
* Vasily Yaremchuk - [yaremchuk](https://www.drupal.org/u/yaremchuk)
