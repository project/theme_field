<?php

namespace Drupal\theme_field\Plugin\Field\FieldWidget;

use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'theme_field_widget' widget.
 *
 * @FieldWidget(
 *   id = "theme_field_widget",
 *   label = @Translation("Theme"),
 *   field_types = {
 *     "theme_field_type"
 *   }
 * )
 */
class ThemeFieldWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The theme handler.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * Constructs the field widget instance.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $theme_handler
   *   The theme handler.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    ThemeHandlerInterface $theme_handler,
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->themeHandler = $theme_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ): self {
    return new self(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('theme_handler'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $default_value = $items[$delta]->value ?? '';

    $options = ['_none' => $this->t('Default theme')];
    $themes = $this->themeHandler->listInfo();
    $theme_options = [];
    foreach ($themes as $theme_name => $theme_info) {
      if (!empty($theme_info->status)) {
        $theme_options[$theme_name] = $theme_info->info['name'];
      }
    }
    array_multisort($theme_options, SORT_NATURAL);
    $element += [
      '#type' => 'select',
      '#options' => $options + $theme_options,
      '#default_value' => $default_value,
      '#multiple' => FALSE,
    ];

    return ['value' => $element];
  }

}
